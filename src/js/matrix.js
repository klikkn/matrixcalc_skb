function Matrix(rows, cols, selector) {

    var _matrix = document.getElementById('matrix-' + selector);
    var _selector = selector;
    var _rows = 0;
    var _cols = 0;

    var cell_value = "";

    _matrix.addEventListener("focus", function(event) {

        var target = event.target;
        if (target.tagName != 'INPUT') return;
        sidebar.setAttribute("data-view", "enter");

        cell_value = target.value;
        target.value = "";

    }, true);

    _matrix.addEventListener("blur", function(event) {

        var target = event.target;
        if (target.tagName != 'INPUT') return;
        sidebar.setAttribute("data-view", "custom");

        if (!correctValue(target.value)) {
            target.value = cell_value;
        }

    }, true);

    var correctValue = function(value) {
        if (!isNaN(value) && +value >= 1 && +value <= 10) return true;
        return false;
    }

    this.getRowsCount = function() {
        return _rows;
    };

    this.getColsCount = function() {
        return _cols;
    };

    this.getSelector = function() {
        return _selector;
    };

    //Display matrix in index.html
    this.init = function() {
        if (_matrix.firstChild) {
            _matrix.removeChild(_matrix.firstChild);
        }

        _cols = cols;
        for (var i = 0; i < rows; i++) {
            this.addTableRow();
        };
    };

    //Add row in matrix
    this.addTableRow = function() {
        var row = _matrix.insertRow();
        _rows++;

        for (var i = 0; i < _cols; i++) {
            addCellInRow(row, _rows, i + 1);
        };
    };

    //Add column in matrix
    this.addTableColumn = function() {
        _cols++;
        for (var i = 0; i < _rows; i++) {
            addCellInRow(_matrix.rows[i], i + 1, _cols);
        };
    };

    var addCellInRow = function(row, i, j) {
        var cell = row.insertCell(-1);
        var text_field = document.createElement('input');
        text_field.setAttribute('type', 'text');
        text_field.value = _selector + i + ',' + j;

        if (selector == 'c') {
            text_field.setAttribute('disabled', 'disabled');
        }

        cell.appendChild(text_field);


    };

    this.deleteTableRow = function() {
        _matrix.deleteRow(-1);
        _rows--;
    };

    //Add column in matrix
    this.deleteTableColumn = function() {
        for (var i = 0; i < _rows; i++) {
            _matrix.rows[i].deleteCell(-1);
        };

        _cols--;
    };

    this.getTbody = function() {
        return _matrix.firstChild.cloneNode(true);
    };

    this.replaceTbody = function(tbody) {
        var _tbody = _matrix.firstChild;
        _matrix.replaceChild(tbody, _tbody);

        _rows = _matrix.rows.length;
        _cols = _matrix.rows.item(0).cells.length;
    };

    this.clearMatrix = function() {
        for (var i = 0; i < _rows; i++) {
            for (var j = 0; j < _cols; j++) {
                this.setCellValue(i, j, _selector + (i + 1) + ',' + (j + 1));
            }
        }
    };

    this.checkMatrixValues = function() {
        for (var i = 0; i < _rows; i++) {
            for (var j = 0; j < _cols; j++) {
                var value = this.getCellValue(i, j);
                if (!correctValue(value)) return false;
            }
        }
        return true;
    }

    this.getCellValue = function(row, column) {
        return +_matrix.rows[row].cells[column].firstChild.value;
    }

    this.setCellValue = function(row, column, value) {
        _matrix.rows[row].cells[column].firstChild.value = value;
    }


}