function Sidebar(elem) {

    var matrix = matrix_a;
    var cols = matrix.getColsCount();
    var rows = matrix.getRowsCount();
    var self = this;

    var radio_a = document.getElementById("checked-a");
    var radio_b = document.getElementById("checked-b");

    elem.onclick = function(e) {
        var target = e.target;
        var action = target.getAttribute('data-action');
        if (action) {
            self[action]();
        }
    };

    sidebar.addEventListener("focus", function(event) {
        var target = event.target;
        if (target.tagName != 'LABEL') return;

        target.onkeypress = function(event) {
            if (event.keyCode != 13) return;

            var id = target.getAttribute("for");

            if (id == "checked-a") {
                radio_a.setAttribute("checked","");
                radio_b.removeAttribute("checked");
                self.set_matrix_a();
            } else {
                radio_b.setAttribute("checked","");
                radio_a.removeAttribute("checked");
                self.set_matrix_b();
            }
        }

    }, true);

    var updateMatrixCounts = function() {
        cols = matrix.getColsCount();
        rows = matrix.getRowsCount();
    }

    this.set_matrix_a = function() {
        matrix = matrix_a;
        console.log("a:")
    };

    this.set_matrix_b = function() {
        matrix = matrix_b;
        console.log("b:")
    };

    this.add_row = function() {
        updateMatrixCounts();

        if (rows == 10) return;
        matrix.addTableRow();

        if (matrix.getSelector() == 'a') {
            matrix_c.addTableRow();
        }
    };

    this.add_column = function() {
        updateMatrixCounts();

        if (cols == 10) return;
        matrix.addTableColumn();

        if (matrix.getSelector() == 'b') {
            matrix_c.addTableColumn();
        }
    };

    this.delete_row = function() {
        updateMatrixCounts();

        if (cols * rows == 2 || rows == 1) return;
        matrix.deleteTableRow();

        if (matrix.getSelector() == 'a') {
            matrix_c.deleteTableRow();
        }
    };

    this.delete_column = function() {
        updateMatrixCounts();

        if (cols * rows == 2 || cols == 1) return;
        matrix.deleteTableColumn();

        if (matrix.getSelector() == 'b') {
            matrix_c.deleteTableColumn();
        }
    };

    this.mul = function() {
        elem.setAttribute("data-view", "custom");

        if (matrix_a.getColsCount() == matrix_b.getRowsCount()) {
            if (matrix_a.checkMatrixValues() && matrix_b.checkMatrixValues()) {

                for (var i = 0; i < matrix_a.getRowsCount(); i++) {
                    for (var j = 0; j < matrix_b.getColsCount(); j++) {
                        matrix_c.setCellValue(i, j, 0);

                        for (var k = 0; k < matrix_b.getRowsCount(); k++) {
                            matrix_c.setCellValue(i, j, matrix_c.getCellValue(i, j) + matrix_a.getCellValue(i, k) * matrix_b.getCellValue(k, j));
                        }
                    }
                }

            } else {
                console.log("values error");
            }
        } else {
            elem.setAttribute("data-view", "error");
        }
    };

    this.clear = function() {
        matrix_a.clearMatrix();
        matrix_b.clearMatrix();
        matrix_c.clearMatrix();
    };

    this.replace = function() {
        var tbody_a = matrix_a.getTbody();
        var tbody_b = matrix_b.getTbody();

        matrix_a.replaceTbody(tbody_b);
        matrix_b.replaceTbody(tbody_a);

        var rows = matrix_a.getRowsCount();
        var cols = matrix_b.getColsCount();

        matrix_c = new Matrix(rows, cols, 'c');
        matrix_c.init();
    };
}